Commands to compile the documentation

    noweave -index -latex vimrefdoc.nw > vimrefdoc.tex

To extract machine source code:

    notangle -Rmacros.tex vimrefdoc.nw > macros.tex

