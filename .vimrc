" file: .vimrc
" author: Sam Qasbah
" version: 0.1.1
" revision date: 2013/05/12
"
" impostazioni visuali
set columns=82 lines=49
colorscheme murphy
"
" ricerca incrementale
set incsearch
"
" raggiunta la fine di una riga va all'inizio
" della successiva, e viceversa, sia in modo
" inserimento che terminale.
set whichwrap=b,s,<,>,[,]
"
" indentazione automatica con riconoscimento
" automatico del tipo di file
set autoindent
filetype plugin indent on
"
" le linee di testo vengono spezzate due
" caratteri prima di raggiungere il bordo
" della finestra.
" (wm => wrap magin)
set wm=2
"
" alcuni comandi utili per la
" new vim reference card
noremap <leader>t i\topic<Space><Enter>\description<Space><Esc>kA
noremap <leader>s i\section<Space>
noremap <leader>ss i\subsection<Space>
